## Differences
The gravity_object_t interface only has two members, isa and gc.  
Original way obj->isa to get type is replaced by OBJECT_CORE(obj)->isa.  
Partial bridge interfaces send gravity_object_t pointer instead of xdata pointer.  
Additional OBJECT_AS_* macros.  
VALUE_AS_* macros would check type at runtime.  
Makefile target "slib" provides static library.  
Makefile target "test" runs unit tests under test/unittest.  

## License
MIT license for modifications.
