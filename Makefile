COMPILER_DIR = src/compiler
RUNTIME_DIR = src/runtime
SHARED_DIR = src/shared
UTILS_DIR = src/utils
UNITTEST_DIR = test/unittest
OPT_DIR = src/optionals
GRAVITY_SRC = src/cli/gravity.c

CC ?= gcc
AR ?= ar
SRC = $(wildcard $(COMPILER_DIR)/*.c) \
      $(wildcard $(RUNTIME_DIR)/*.c) \
      $(wildcard $(SHARED_DIR)/*.c) \
      $(wildcard $(UTILS_DIR)/*.c) \
      $(wildcard $(OPT_DIR)/*.c)

INCLUDE = -I$(COMPILER_DIR) -I$(RUNTIME_DIR) -I$(SHARED_DIR) -I$(UTILS_DIR) -I$(OPT_DIR)
CFLAGS = $(INCLUDE) -std=gnu99 -fgnu89-inline -fPIC -DBUILD_GRAVITY_API
OBJ = $(SRC:.c=.o)

ifeq ($(OS),Windows_NT)
	# Windows
	EXECTARGET = gravity.exe
	LIBTARGET = gravity.dll
	SLIBTARGET = gravity.lib
	LDFLAGS = -lm -lShlwapi
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		# MacOS
		EXECTARGET = gravity
		LIBTARGET = libgravity.dylib
		SLIBTARGET = libgravity.a
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),OpenBSD)
		# OpenBSD
		EXECTARGET = gravity
		# LIBTARGET = libgravity.so (not used)
		CFLAGS += -D_WITH_GETLINE
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),FreeBSD)
		# FreeBSD
		EXECTARGET = gravity
		# LIBTARGET = libgravity.so (not used)
		CFLAGS += -D_WITH_GETLINE
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),NetBSD)
		# NetBSD
		EXECTARGET = gravity
		# LIBTARGET = libgravity.so (not used)
		CFLAGS += -D_WITH_GETLINE
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),DragonFly)
		# DragonFly
		EXECTARGET = gravity
		# LIBTARGET = libgravity.so (not used)
		CFLAGS += -D_WITH_GETLINE
		LDFLAGS = -lm
	else
		# Linux
		EXECTARGET = gravity
		LIBTARGET = libgravity.so
		SLIBTARGET = libgravity.a
		LDFLAGS = -lm -lrt
	endif
endif

ifeq ($(mode),debug)
	CFLAGS += -g -O0 -DDEBUG
else
	CFLAGS += -O2
endif

all: gravity

gravity: $(OBJ) $(GRAVITY_SRC)
	$(CC) $(CFLAGS) -o $(EXECTARGET) $^ $(LDFLAGS)

.PHONY: all clean gravity $(UNITTEST_DIR) $(UNITTEST_SUBDIR)

lib: gravity
	$(CC) -shared -o $(LIBTARGET) $(OBJ) $(LDFLAGS)

slib: gravity
	$(AR) rcs $(SLIBTARGET) $(OBJ)
	
test: $(UNITTEST_DIR)

$(UNITTEST_DIR): gravity
	./$(EXECTARGET) -t $(UNITTEST_DIR)

clean:
	rm -f $(OBJ) $(EXECTARGET) $(LIBTARGET) $(SLIBTARGET)
